<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>form input</title>
</head>
<body>
    <h1>halaman form input kamar</h1>
    @if (session('error'))
    {{ session('error') }}
@endif
@if(session('sucses'))
    {{ session('sucses') }}
@endif
    <form action="{{ route('kamar.simpan-data') }}" method="POST">
        @csrf
        <label for="">Nama kamar</label><br>
        <input type="text" name="nama_kamar" id="nama_kamar" value="{{ old('nama_kamar') }}" /> <br>
        <label for="">Kapasitas</label> <br>
        <input type="text" name="kapasitas" id="kapasitas" value="{{ old('kapasitas') }}"><br>
        <input type="submit" value="Simpan">
        <a href="{{ route('kamar.list') }}">kembali</a>
    </form>
</body>
</html>
