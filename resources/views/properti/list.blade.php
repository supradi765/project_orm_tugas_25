<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>List kamar</title>
</head>
<body>
    <h1>List kamar</h1>
    <a href="{{ route('properti.form-input') }}">Tambah Data</a>
    <table border="1" width="100%">
        <tr>
            <th>No.</th>
            <th>Nama Kamar</th>
            <th>Nama Proprti</th>
            <th>Jumlah</th>
        </tr>
        @php $no=1; @endphp
        @foreach($dataProperti as $row)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $row->nama_kamar }}</td>
            <td>{{ $row->nama_properti }}</td>
            <td>{{ $row->jumlah_properti }}</td>
        </tr>

        @endforeach
    </table>
</body>
</html>
