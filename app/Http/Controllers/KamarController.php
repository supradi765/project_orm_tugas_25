<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kamar;

class KamarController extends Controller
{
    public function listKamar(){
        $dataKamar = Kamar::select('nama_kamar', 'kapasitas')->get();
        return view('kamar.list', compact('dataKamar'));
    }

    public function formInput(){
        return view('kamar.form_input');
    }

    public function simpanData(Request $req) {
        try {
            $datas = $req->all();
            $save = new Kamar;
            $save->nama_kamar = $datas['nama_kamar'];
            $save->kapasitas = $datas['kapasitas'];
            $save->save();
            return redirect()->route('kamar.list')->with('suscces', _('berhasil'));
        }  catch (\Throwable $th) {
            return redirect()->route('kamar.list')->with('error', __($th->getMessage()));
        }
    }

}
